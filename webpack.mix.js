let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/main.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .extract(['vue', 'jquery']);


mix.combine([
    'resources/assets/js/arrive.min.js',
    'resources/assets/js/demo.js',
    'resources/assets/js/chartist.min.js',
    'resources/assets/js/material.min.js',
    'resources/assets/js/material-dashboard.js'
], 'public/js/main.js');


mix.combine([
    'resources/assets/css/bootstrap.min.css',
    'resources/assets/css/demo.css',
    'resources/assets/css/material-dashboard.css'
], 'public/css/app.css');